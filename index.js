const API_KEY = "f32d149fcc9e6bd1097086aea0b123d7";
const url = `https://gnews.io/api/v4/top-headlines?apikey=${API_KEY}`;

window.addEventListener('load', () => fetchNews("india"));

function reload() {
   window.location.reload();
}

async function fetchNews(query) {
   try {
       const res = await fetch(`${url}&country=${query}`);
       const data = await res.json();
       console.log(data);
       if (data.articles) {
           bindData(data.articles);
       } else {
           console.error("No articles found in the response.");
       }
   } catch (error) {
       console.error("Error fetching news:", error);
   }
}

function bindData(articles) {
   const cardsContainer = document.getElementById('cards-container')
   const newsCardTemplate = document.getElementById('template-news-card');

   cardsContainer.innerHTML = '';

   articles.forEach(article => {
      if(!article.image) return;
      const cardClone = newsCardTemplate.content.cloneNode(true);
      fillDataInCard(cardClone, article);
      cardsContainer.appendChild(cardClone);
   })
}

function fillDataInCard(cardClone, article) {
   const newsImg = cardClone.querySelector('#news-img');
   const newsTitle = cardClone.querySelector('#news-title');
   const newsSource = cardClone.querySelector('#news-source');
   const newsDesc = cardClone.querySelector('#news-desc');

   newsImg.src = article.image;
   newsTitle.innerHTML = article.title;
   newsDesc.innerHTML = article.description;

   const date = new Date(article.publishedAt).toLocaleDateString("en-US", {
       timeZone: "Asia/Jakarta"
   });

   newsSource.innerHTML = `${article.source} . ${date}`;

   cardClone.firstElementChild.addEventListener('click', () => {
       window.open(article.url, "_blank");
   })
}

let curSelectedNav = null;
function onNavItemClick(id) {
   fetchNews(id);
   const navItem = document.getElementById(id);
   curSelectedNav?.classList.remove('active');
   curSelectedNav = navItem;
   curSelectedNav.classList.add('active');
}

const searchButton = document.getElementById('search-button');
const searchText = document.getElementById('search-text');

searchButton.addEventListener('click', () => {
   const query = searchText.value; // Use value property instead of ariaValueMax
   if(!query) return;
   fetchNews(query);
   curSelectedNav?.classList.remove('active');
   curSelectedNav = null;
});
